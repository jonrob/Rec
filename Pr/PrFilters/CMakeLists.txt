###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Pr/PrFilters
---------------
#]=======================================================================]

gaudi_add_header_only_library(PrFiltersLib
    LINK
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        Rec::FunctorCoreLib
        Rec::SelKernelLib
        LHCb::DetDescLib
        LHCb::EventBase
)

gaudi_add_module(PrFilters
    SOURCES
        src/PrCheckEmptyFilter.cpp
        src/PrGECFilter.cpp
        src/PrFilterIPSoA.cpp
        src/PrFilter.cpp
        src/PrFilterTrackRelation.cpp
    LINK
        PrFiltersLib
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::FTDAQLib
        LHCb::UTDAQLib
        LHCb::LHCbKernel
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::PrKernel
)
