!------------------------------------------------------------------------------
! Package     : CaloFuture/CaloFuturePIDs
! Responsible : Olivier Deschamps odescham@in2p3.fr
! Purpose     : CaloFuturerimeter PIDs 
!------------------------------------------------------------------------------

! 2016-08-16 - Olivier Deschamps
  - implement counter() switch based on FutureCounterLevel tool

!========================= CaloFuturePIDs v5r22 2016-03-17 =========================
! 2016-03-05 - Olivier Deschamps
  - CaloFuturePIDs configurable :  add slots to configure the Accepted Track::Types 
  - src : add counter per track type in match algs

! 2016-02-24 - Olivier Deschamps
  - fix clang warning in friend declarations of ToolFactory (class->struct)

!========================= CaloFuturePIDs v5r21 2016-01-28 =========================
! 2016-25-01 - Olivier Deschamps
 - CaloFuturePhotonMatch,CaloFutureElectronMatch, CaloFutureBremMatch tools : 
        - move m_position and m_plane data members to the base tool (CaloFutureTrackMatch) and invoke a BeginEvent reset 
        - this fixes the (rare) initialization issues producing difference in the offline versus online reconstruction

!========================= CaloFuturePIDs v5r20p1 2015-10-13 =========================
! 2015-08-12 - Gerhard Raven
 - remove #include of obsolete Gaudi headers

!========================= CaloFuturePIDs v5r20 2014-11-05 =========================
! 2014-11-04 - Dima Golubkov
  - CaloFutureElectronMatch : change back to extrapolation to the ZShowerMax plane

!========================= CaloFuturePIDs v5r19 2014-09-08 =========================
! 2014-07-25 - Dima Golubkov
  - CaloFutureElectronMatch, CaloFutureBremMatch.cpp, CaloFuturePhotonMatch.cpp : decrease verbosity of tolerable errors to WARNING

! 2014-07-23 - Olivier Deschamps for Dima
  - CaloFutureElectronMatch : extrapolate the track to the CaloFutureHypo Z position instead of ZShowerMax plane

!========================= CaloFuturePIDs v5r18p1 2014-07-14 ========================
! 2014-06-27 - Olivier Deschamps
 - Fix minor typo in a printout

!========================= CaloFuturePIDs v5r18 2014-06-10 =========================
! 2014-05-28 - Dmitry Golubkov
 - CaloFutureTrackMatch.h : remove unused i_updateField() declaration

! 2014-05-28 - Marco Cattaneo
 - Fix some unprotected StatusCodes in CaloFutureTrackMatch.h

!========================= CaloFuturePIDs v5r17 2014-05-13 =========================
! 2014-05-06 - Marco Cattaneo
 - Remove debug() from CaloFutureTrackAlg::_setProperty and CaloFutureTrackTool::_setProperty
   This function is called in the constructor, when the OutputLevel is not initialised

! 2014-04-15 - Oleg Stenyakin
 - CaloFutureTrackMatch : fix warnings

! 2014-04-14 - Oleg Stenyakin
 - CaloFutureTrackMatch.{h,cpp} : the X-correction for the e+, e- track-cluster  matching
   for each zone of the ECAL is added. Parameters of the correction are loaded
   from the CondDB by default, added protection if the Conditions are missing.

!========================= CaloFuturePIDs v5r16 2014-03-18 =========================
! 2014-03-06 - Olivier Deschamps
 -  python/Configuration.py : propagate OutputLevel value to calo sequences only when the property is explicity set

!========================= CaloFuturePIDs v5r15 2013-10-24 =========================
! 2013-10-09 - Olivier Deschamps
 -  python/CaloFuturePIDs/*.py : 
        - possibility to set-up a configuration without active Spd/Prs  (NoSpdPrs = <False> flag)
        - see CaloFutureReco releases.notes for detail

!========================= CaloFuturePIDs v5r14 2013-06-03 =========================
! 2013-05-13 - Marco Cattaneo
 - Fix include paths following previous change
 - Remove 'do nothing' finalize methods

! 2013-05-10 - Olivier Deschamps
  - move CaloFutureTrackAlg/Match/Tool.{cpp,h} from CaloFutureUtils

!========================= CaloFuturePIDs v5r13 2012-11-28 =========================
! 2012-11-22 - Marco Clemencic
 - Added CMake configuration file.

! 2012-10-17 - Marco Cattaneo
 - In CaloFutureTrack2IDAlg, return Error from initialize if undefined Input, removes
   need for Assert in execute
 - Fix trivial icc remarks

! 2012-10-08 - Marco Cattaneo
 - In Configuration.py, remove setting of MeasureTime=true in sequencers,
   should not be the default, and should in any case use TimingAuditor.

!========================= CaloFuturePIDs v5r12 2012-03-27 =========================
! 2012-03-15 - Marco Cattaneo
 - Fix unprotected debug() MsgStreams
 - Fix trivial icc remarks
 - Fix UNINIT_CTOR defects

!========================= CaloFuturePIDs v5r11 2011-12-15 =========================
! 2011-12-09 - Olivier Deschamps 
  - CaloFuturePIDs/PIDs.py : fix external cluster setup

!========================= CaloFuturePIDs v5r10 2011-11-08 =========================
! 2011-09-30 - Olivier Deschamps for Victor Coco
  - CaloFuturePhotonMatch : fix Hcal matching setup

!========================= CaloFuturePIDs v5r9 2011-04-27 =========================
! 2011-04-24 - Olivier Deschamps
  - update CaloFutureProcessor configurable with 'ExternalClusters' slot

!========================== CaloFuturePIDs v5r8 2010-09-24 ========================
! 2010-09-20 - Olivier Deschamps
  - CaloFuturePhotonIdALg :  fix unchecked StatusCode

! 2010-09-01 - Olivier Deschamps
  - fix compilation warning on windows

! 2010-08-27 - Olivier Deschamps
  - python/PID.py : new class referencePDF() to feed HistogramDataSvc with default THS PDFs
  - CaloFuturePhotonIDAlg : simplify using the new CaloFutureHypoEstimator tool

!========================== CaloFuturePIDs v5r7 2010-08-25 ========================
! 2010-08-17 - Olivier Deschamps
 - new version of CaloFuturePhotonIDAlg (get data from condDB or THS) from F. Machefert 
 - python/PIDs.py : remove default hardcoded neutralID PDF
 - python/Configuration.py : add PIDList to select the PID component(s) to be added to the caloPIDs sequence


!========================== CaloFuturePIDs v5r6p1 2010-06-21 ========================
! 2010-06-04 - Rob Lambert
 - Fixes for some windows warnings savannah 15808

!========================== CaloFuturePIDs v5r6 2010-05-21 =========================
! 2010-05-20 - Olivier Deschamps
 - python/Configuration.py : reduce verbosity


! 2010-05-19 - Gerhard Raven
 - only summarize 'match' errors, do not print (see https://savannah.cern.ch/bugs/?67077) 

!========================== CaloFuturePIDs v5r5 2010-03-19 =========================
! 2010-03-12 - Dmitry Golubkov
 - CaloFutureID2DLL - enable CondDB by default, add protection if the Condition is missing
 - BremPIDeAlg, FutureEcalPIDeAlg, FutureEcalPIDmuAlg, FutureHcalPIDeAlg, FutureHcalPIDmuAlg, FuturePrsPIDeAlg - set default Condition and histogram names


! 2010-03-08 - Olivier Deschamps
 - Configurables : 
   - adapt to changes in CaloKernel/ConfUtils
   - make all component context-dependent
   - add few slots (SkipNeutral, SkipCharged )
   - add new components for neutral PIDs


 - options :
   - clean options (remove obsolete) -> options directory is now empty

 - src :
   - new algorithm CaloFuturePhotonIdALg : produce <Hypo,Likelihood> relation table -> NeutralProtoPALg
      -> remove obsolete CaloFuturePhotonEstimatorTool and CaloFutureSingleGammaTool
   - use TrackRungeKuttaExtrapolator by default everywhere
   - add counters to monitor algorithms I/O 

 - TODO : 
   - CaloFuturePIDsConf : split the sequence by CaloFuturePIDs technique and add PIDsList configuration (to be synchronized with CaloFutureReclo/Processor)


! 2010-02-28 - Olivier Deschamps
  - PhotonMatchAlg : remove forgotten lines overwritting the context dependent TES I/O

!========================== CaloFuturePIDs v5r4 2010-02-15 =========================
! 2009-11-17 - Olivier Deschamps
 - PIDs.py : use RungeKutta extrapolator instead of HeraB for HLT processing

! 2010-02-08 - Dmitry Golubkov
 - CaloFutureID2DLL - optionally read the DLLs from CondDB
 - cmt/requirements - version incremented to v5r4

!========================== CaloFuturePIDs v5r3 2010-01-21 =========================
! 2010-01-13 - Marco Cattaneo
 - Follow Gaudi fix #61116: GaudiCommon::Exception now returns void

!========================== CaloFuturePIDs v5r2 2009-11-13 =========================
! 2009-11-13 - Rob Lambert
 - Tagged the package

! 2009-10-30 - Olivier Deschamps
 
 -  fix unchecked StatusCode


! 2009-10-30 - Vanya Belyaev

 -  suppress few warnigs 

! 2009-10-25 - Vanya Belyaev

 - add ICaloFutureDigit4Track interface for all <CALOFUTURE>EnergyForTrack tools 

 - CaloFutureEnergyForTrack.cpp

    fix the typo in  property name 

 - cmt/requirements
   
     version increment to v5r2 

!========================== CaloFuturePIDs v5r1p1 2009-09-30 =========================
! 2009-09-30 - Olivier Deschamps
 - reduce CaloFutureTrackMatchAlg verbosity 

! 2009-09-03 - Olivier Deschamps 
 - reduce InCaloFutureAcceptance/CaloFutureTrackMatchAlg verbosity when no good tracks found (warning->debug)
 - add protection against missing inputs

!========================== CaloFuturePIDs v5r1 2009-09-03 =========================
! 2009-09-03 - Marco Cattaneo
 - Remove obsolete file src/CaloFuturePIDs_dll.cpp

! 2009-09-01 - Vanya BELYAEV
 - suppress warnings for consigurables 
 - cmt/requirements 
      version increment to v5r1 

!========================== CaloFuturePIDs v5r0 2009-08-31 =========================
! 2009-08-21 - Olivier Deschamps
  - implement generic context-dependent TES I/O

! 2009-08-10 - Vanya BELYAEV

 - polish the configurables 

! 2009-08-05 - Vanya BELYAEV

 - add the configurables 
 - version increment to v5r0 

!========================== CaloFuturePIDs v4r17 2009-05-25 =========================
! 2009-05-15 - Marco Cattaneo
 - Fix untested StatusCode on Warning() and Error() calls
 - Replace endreq with endmsg

!========================== CaloFuturePIDs v4r16 2009-05-08 =========================
! 2009-04-16 - Olivier Deschamps
 - fix unchecked StatusCode

!========================== CaloFuturePIDs v4r15 2009-03-11 =========================
! 2009-03-11 - Victor Egorychev
 - AddNeigbours in FutureEcalEnergyForTrack and HcalEnrgyForTrack changed to FALSE

!========================== CaloFuturePIDs v4r14p1 2008-01-12 =======================
! 2008-12-10 - Marco Cattaneo
 - Fix gcc 4.3 compilation warnings

!========================== CaloFuturePIDs v4r14 2008-11-21 =========================
! 2008-10-10 - Olivier Deschamps
 - fix unitialized variable in CaloFutureID2DLL.cpp

!========================== CaloFuturePIDs v4r13 2008-07-18 =========================
! 2008-07-17 - Olivier Deschamps
 - duplicate PhotonPDF.opts for Hlt usage (HltPhotonPDF.opts)

!========================== CaloFuturePIDs v4r12 2008-07-02 =========================
! 2008-06-27 - Olivier Deschamps
 - CaloFutureTrackMatchAlg and inCaloFutureAcceptanceAlg : protect against empty track container
 - use default HLT locations for the HLT context in all algorithm/tool


! 2008-06-26 - Juan PALACIOS
 - cmt/requirements
  . Increase version to v4r12
 - src/CaloFuturePhotonEstimatorTool.cpp
 - src/CaloFutureSingleGammaTool.cpp
 - src/Linear.h
  . Change all Gaudi::XYZLine and Gaudi::Line for Gaudi::Math::XYZLine and
    Gaudi::Math::XYZLine respectively (adapting to Kernel/LHCbMath v3)

!========================== CaloFuturePIDs v4r11 2008-06-04 =========================
! 2008-06-04 - Marco Cattaneo
 - Fix doxygen warning

! 2008-06-03 Olivier Deschamps
 - change incident type EndEvent to BeginEvent in CaloFutureEnergyForTrack.cpp

!========================== CaloFuturePIDs v4r10 2008-05-13 =========================
! 2008-05-13 Olivier Deschamps
- restore void _setProperty(..) instead of StatusCode setProperty(...)

!========================== CaloFuturePIDs v4r9 2008-01-24 ===================
! 2008-01-24 - Victor Egorychev
- _setProperty was changed to setProperty
- fixed mistypo with AddNeigbours instead AddNeibours
- version was incremented to v4r9

!========================== CaloFuturePIDs v4r8 2007-09-20 ===================
! 2007-08-24 - Olivier Deschamps
 - Fix most of the unchecked StatusCodes

!========================== CaloFuturePIDs v4r7 2007-05-31 ===================
! 2007-05-31 - Marco Cattaneo
 - Fix doxygen warnings

! 2007-05-26 - Victor Egorychev
- DLL hist (DC06) for different BremPID added, needs ParamFiles v6r1 
- src/BremPIDeAlg.cpp
  - new PIDs from DC06 added
 - cmt/requirements 
    version increment to v4r7

!========================== CaloFuturePIDs v4r6 2007-03-02 ==========================
! 2007-03-02 - Marco Cattaneo
 - Remove LHCbDefinitions includes
 - Remove obsolete CaloFuturePIDs_load.cpp file
 - Fix doxygen warnings and other minor cleanups

!========================== CaloFuturePIDs v4r5p1 2006-11-27 ===================
! 2006-11-27 - Victor Egorychev
 - src/CaloFutureID2DLL.cpp Warning " ... very priliminary version ... " was
   removed

!========================== CaloFuturePIDs v4r5 2006-11-07 ===================
! 2006-11-07 - Marco Cattaneo
 - Change CALOFUTUREPIDOPTS variable to CALOFUTUREPIDSOPTS, to be standard....

! 2006-11-06 - Victor Egorychev
- DLL hist (DC06) for different types tracks for PIDe/mu added 
  (Ecal, Hcal, Prs). For BremPID - still old hist
- src/FutureEcalPIDeAlg.cpp
  - new PIDs from DC06 added for Long, Downstream, TTrack tracks
- src/FutureHcalPIDeAlg.cpp
  - new PIDs from DC06 added for Long, Downstream, TTrack tracks
- src/FuturePrsPIDeAlg.cpp
  - new PIDs from DC06 added for Long, Downstream, TTrack tracks
- src/FutureEcalPIDmuAlg.cpp
  - new PIDs from DC06 added for Long, Downstream, TTrack tracks
- src/FutureHcalPIDmuAlg.cpp
  - new PIDs from DC06 added for Long, Downstream, TTrack tracks
- src/BremPIDeAlg.cpp
  - old PIDs added for Long, Velo, Upstream tracks
- src/CaloFutureID2DLL.cpp
  - hist for different track types added
- src/CaloFutureID2DLL.h
  - hist for different track types added
 - cmt/requirements 
    version increment to v4r5 

! ========================= CaloFuturePIDs v4r4 2006-09-28 ===================
! 2006-09-28 - Victor Egorychev
 - src/Linear.h
    bug fix for invalid linear state extrapolation 
 - cmt/requirements 
    version increment to v4r4 

! ========================= CaloFuturePIDs v4r3 2006-08-03 ===================
! 2006-08-03 - Chris Jones
 - Add missing data initialisations in CaloFutureElectronMatch and CaloFuturePhotonMatch

!========================== CaloFuturePIDs v4r2 2006-07-19 ===================
! 2006-07-19 - Marco Cattaneo
 - ToString.h : remove all templates now in GaudiKernel/ToStream.h

!========================== CaloFuturePIDs v4r1 2006-06-27 ===================
! 2006-06-27 - Olivier Deschamps 
  - CaloFutureReco/CaloFuturePIDs repackaging :
    - add CaloFutureSingleGammaTool/CaloFuturePhotonEstimatorTool from CaloFutureReco/
    - increment the release number in cmt/requirements 

!========================== CaloFuturePIDs v4r0 2006-06-22 ===================
! 2006-06-22 - Vanya BELYAEV
 - fix the inconsistencies in the configuration of 
   accepted types for "Brem"-related algorithms 

! 2006-06-21 - Vanya BELYAEV
 - fix the problem for "Brem"-tools.
   now it tries to get the fixed states:
      AtTT, EndRich1, BegRich1, EndVelo, 
   othwrwise it used the state nearest to 2*meter (and x<4*meter).
   Also use the explicite extrapolation
 - Linear.h  - explicite linear extrapolator. 
    The speed-up of BremMatch algorthm is approximately 2 times "9"->"4"

! 2006-06-20 - Olivier Deschamps
 - minor update (RelationWeighted1D -> 2D to please ChargedProtoPAlg)

! 2006-06-19 - Vanya Belyaev
 - First version for DC06

!========================== CaloFuturePIDs v3r0 2005-11-04 ===================
! 2005-11-04 - Olivier Deschamps
 - Adapt to new Track Event Model (TrackEvent v1r4)

  modified file :  
   src/CaloFutureTrackPrsEval.h/cpp
   src/CaloFutureTrackHcalEval.h/cpp
   src/CaloFutureTrackEval.h/cpp
   src/CaloFutureTrackEcalEval.h/cpp
   src/CaloFutureTrack2IdAlg.cpp
   src/CaloFutureTrack2EstimatorAlg.cpp
   src/CaloFuturePIDsData.cpp
   src/CaloFuturePhotonEstimatorTool.h/cpp

 - cmt/requirements 
   version increment to v3r0

!========================= CaloFuturePIDs v2r6 2005-06-02 ===========================
! 2005-06-02 - Marco Cattaneo
 - Adapt Brunel.opts to new phase name Reco instead of BrunelReco

!========================= CaloFuturePIDs v2r5p1 2005-05-23 =========================
! 2005-05-10 - Vanya BELYAEV

 - src/CaloFuturePIDsData.cpp
   1) rename "match" -> "mat" as N-Tuple item name to please PAW 
                                     (thanks to Kirill VORONCHEV) 
   2) add photon match estimator  
   3) cosmetic modifications 

 - src/CaloFuturePIDsData.h 
    remove the file 
 
!========================= CaloFuturePIDs v2r5 2005-05-09 ===========================
! 2005-05-08 - Vanya BELYAEV
    Eliminate all *associators*  
    Now everything works directly with relation tables 
    (Assuming the proper instrumentation of "Data-On-Demand" service)
 
 - options/CaloFuturePIDsOnDemand.opt
     new configuration file (TEST-PHASE) for "CaloFuturePIDs-On-Demand"
     approach
 - cmt/requirements 
     version increment to v2r5 

!======================= CaloFuturePIDs v2r4p1 2005-03-08 =====================
! 2005-03-08 - Marco Cattaneo
 - Fix some doxygen warnings
 - Remove unmatched "#pragma print on" in options to avoid resetting print
   level in higher level options files

!======================== CaloFuturePIDs v2r4 2004-09-08 ======================
! 2004-09-02 - Vanya BELYAEV
   make the preparation for HLT/Trigger development 
 - update for modifier CaloFutureInterfaces 
 - cmt/requirements 
   version increment to v2r4 

!======================== CaloFuturePIDs v2r3 2004-04-27 ======================
! 2004-04-27 - Vanya Belyaev
 -  src/CaloFuturePIDsData.cpp
    fix stupid misprint in type name for associator 
 -  cmt/requirements
    increment the version to v2r3 

!======================== CaloFuturePIDs v2r2 2004-04-19 ======================
! 2004-04-19 - Marco Cattaneo
 - Fix doxygen warnings

! 2004-04-18 - Vanya Belyaev
 - src/CaloFuturePhotonEstimatorTool.h,.cpp
   the updated version of PhotonID tool from Frederic Machefert
 - options/PhotonPDF.opts 
   the updated file with parameters 
 - cmt/requirements
   increment the version to v2r2 
 
!======================== CaloFuturePIDs v2r1 2004-03-18 ======================
! 2004-03-17 - Vanya BELYAEV
 - src/CaloFuturePhotonEstimatorTool.h,cpp
   new tool for photon ID estimate from Frederic Machefert
   The origina version from Frederic is udapted a bit to 
   new GaudiTool/CaloFutureTool methods and to 
   "typo"-fix ICaloFutureLikelyhood -> ICaloFutureLikelihood 
 - src/CaloFuturePIDsData.h/.cpp  
   new algorithm to fill NTuple needed to populate 
   the reference histograms for charged particle ID 

 - src/CaloFuturePIDs_load.cpp - add new tool and new 
                           algorithm to the list of 
                           known components 

 - cmt/requirements  increment the version to v2r1 

 - options/PhotonPDF.opts 
   configuration of CaloFuturePhotonEstimatorTool/PhotonPID 
   ********** ATTENTION: tool name is choosen to be 'PhotonPID' *********

!======================== CaloFuturePIDs v2r0 2004-03-08 ======================
! 2004-03-08 - Marco Cattaneo
 - In CaloFutureTrack2Estimator.h/.cpp, CaloFutureTrack2Idalg.h/.cpp
   . rename m_upstream to m_downstream
   . rename UseUpstream property to UseDownstream 
   . use new isDownstream() method of TrStoredTrack instead of upstream()

 - In *.opts
   . rename UseUpstream property to UseDownstream 

! 2004-02-17 - Vanya BELYAEV

   -  update for reading the input histograms from 

      $PARAMFILEROOT/data/CaloFuturePIDs.root  
        or 
      $PARAMFILEROOT/data/CaloFuturePIDs.root 

  -   remove 'text' histograms from options directory 

  -   cmt/requirements increment teh MAJOR version 

!======================== CaloFuturePIDs v1r2 2003-12-11 ======================
! 2003-12-11 - Marco Cattaneo
 - Move to Gaudi v13:
   . requirements:        use CaloFutureInterfaces v3r*
   . CaloFutureTrack2IdAlg.cpp: adapt to AIDA 3.0 

!======================== CaloFuturePIDs v1r1 2003-07-17 ======================
! 2003-07-17 - Ivan BELYAEV
 - src/CaloFutureTrackEval.cpp 
   fix an error (return m_bad valeu for zero energy deposition) 
 - cmt/requirements  increase the version 

! 2003-07-17 - Ivan BELYAEV
 - minor change if algorithms to speed-up a code a little bit 
 - options introduce a low/high limits for estimators to skip 
   'bad' records 

!======================== CaloFuturePIDs v1r0 2003-04-17 ======================
! 2003-04-17 - Marco Cattaneo
 - Remove "#" from "#include" when it appears in doxygen comment lines of job 
   options, as this breaks the production tools

! 2003-03-13 - Vanya BELYAEV
 - new package 
