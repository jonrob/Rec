/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/IAlgTool.h"

namespace MicroDST {

  /** @class ICloner ICloner.h MicroDST/ICloner.h
   *
   *
   *  @author Juan Palacios
   *  @date   2009-07-29
   */

  template <class T>
  struct ICloner : virtual IAlgTool {

    using Type = T;

    /// Clone operator
    virtual T* operator()( const T* source ) const = 0;
  };

} // namespace MicroDST
