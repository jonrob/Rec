2023-09-25 Rec v35r19
===

This version uses
Lbcom [v34r18](../../../../Lbcom/-/tags/v34r18),
LHCb [v54r18](../../../../LHCb/-/tags/v54r18),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r21](../../../../Detector/-/tags/v1r21) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r18](/../../tags/v35r18), with the following changes:

### New features ~"new feature"

- ~Tracking ~"MC checking" | Revive and reimplement PrDebugTrackingLosses, !3425 (@gunther) [#112]


### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~Tracking ~UT | Add support for v6 UT data, !3467 (@graven)
- ~"PV finding" | Adding InteractionRegion information to PV reco algorithms, !3552 (@mgiza)
- ~Monitoring | Add monitor for ECAL energy vs raw size, !3598 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~UT | Convert UT warnings and errors to accumulators., !3564 (@ganowak) [Lbcom#11]
- ~Functors | Fix memory leak: use Particle::Container instead of Particle::ConstVector in the appropriate place, !3572 (@graven)
- ~Monitoring | Fix little typo which can cause confusion when looking at monitoring plots, !3594 (@tmombach)
- ~Build | Fixes for a clean compilation on ARM, !3516 (@clemenci)
- Fixes for LCG 104, !3449 (@clemenci) [LHCBPS-1912]


### Documentation ~Documentation


### Other

- ~Tracking | Assert order of x coordinates in PrStoreSciFiHits, !3557 (@gunther)
- ~RICH | Revert wrong use of 'RichCalibratedDLLX' values, !3590 (@jonrob)
- ~RICH | Add RICH algorithm to cheat hit positions usng MCRichHits, !3587 (@jonrob)
- ~Functors ~Tuples | Extend F.VALUE_OR for more input types, !3489 (@jzhuo)
- ~"MC checking" | Fix the SciFi Hit LHCbID, !3584 (@jzhuo)
- ~Tuples | Algorithm for HLT2 tistos, !3363 (@sstahl)
- Prevent dereferencing of nullptr, !3580 (@clemenci)
- When DOCA functors are operated on basic particles throw an exception with message to user hinting that this might be the case, !3551 (@amathad)
- Fix header stand alone compilation, !3573 (@clemenci)
- Adapt to removal of simdwrapper float's operator==, !3554 (@ahennequ)
