2023-06-12 Rec v35r9
===

This version uses
Lbcom [v34r9](../../../../Lbcom/-/tags/v34r9),
LHCb [v54r9](../../../../LHCb/-/tags/v54r9),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r13](../../../../Detector/-/tags/v1r13) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r8](/../../tags/v35r8), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Build | Fix PrMatchNN compilation on avx512 platforms, !3443 (@gunther)
- Fix use of references to DD4hep handles, !3439 (@clemenci)
- Adapt PrResidualPrUTHits for new UT numbering scheme, !3413 (@decianm)


### Enhancements ~enhancement

- ~Monitoring | Histogram for TAE events for PLUME, !3393 (@vyeroshe)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Monitoring | Fix range for track history histogram, !3452 (@decianm)
- Delete Tr/TrackAssociator since replaced by PrTrackAssociator in PrMCTools, !3435 (@rquaglia)
- Removed deprecated Retina algorithms, !3418 (@dpassaro)
- Remove CombineParticles and MCMatcher, !3405 (@pkoppenb)
- Remove FilterDesktop and selection framework, !3392 (@pkoppenb)


### Other

- ~Configuration ~RICH | RichFutureRecSys ConfiguredRichReco.py - Add proc to return default photon probability cut values, !3408 (@jonrob)
- ~Tracking | Add MinP property to PrMatchNN, !3409 (@jonrob)
- ~RICH | RichCKResolutionFitter: Handle exceptions during string formation, !3440 (@jonrob)
- ~Filters | Add algorithm to match Velo track to decay vertex, !3422 (@mstahl)
- ~Functors | Added RICH DLL functors, !3406 (@imackay)
- ~Functors ~Tuples | Update the Run3 DTF: Add extra output and v2 PVs support, !3349 (@jzhuo)
- Update References for: LHCb!4116, MooreOnline!251, DaVinci!904 based on lhcb-master-mr/8181, !3447 (@lhcbsoft)
- Get particle selection from WeightedRelationTable, !3434 (@tfulghes)
- Include header added., !3428 (@obuon)
- Fix track monitor number of FT hits, !3427 (@lohenry)
- Streamline code for UT tracking code, !3421 (@decianm)
- FT: Remove use of odin.timeAlignmentEventIndex(), !3419 (@jheuel)
- Add minimum momentum cut in hybrid seeding, !3411 (@lohenry)
- Fixes for Gaudi::Functional not using GaudiAlg, !3395 (@clemenci)
- Update References for: Detector!382, LHCb!4096, Allen!1190 based on lhcb-master-mr/7851, !3412 (@lhcbsoft)
- Store T tracks for Phoenix, !3403 (@decianm)
- Improve invalid value handling with trigger decision, !3396 (@amathad)
- Add converter from VPHits to VPLightClusters, !3372 (@ahennequ)
