2023-03-03 Rec v35r5
===

This version uses
Lbcom [v34r5](../../../../Lbcom/-/tags/v34r5),
LHCb [v54r5](../../../../LHCb/-/tags/v54r5),
Detector [v1r9](../../../../Detector/-/tags/v1r9),
Gaudi [v36r11](../../../../Gaudi/-/tags/v36r11) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r4](/../../tags/v35r4), with the following changes:

### New features ~"new feature"

- ~Tracking ~FT | SciFi mat contraction calibration conditions, !2917 (@isanders)


### Fixes ~"bug fix" ~workaround

- ~Tracking | Fix BegRich2->AtT parameterised scatter, !3314 (@ausachov)
- Move ITrackExtrapolator to a separate dictionary so it can be used in..., !3325 (@raaij)
- Fix bug introduced in !3303, !3320 (@graven)
- Fix data race introduced in lhcb/Rec!3293, !3299 (@graven)


### Enhancements ~enhancement

- ~Tracking | Introduce hit patterns to fix outlier removal in PrKalmanFilter, !3192 (@ausachov) [#427]
- ~Functors | Adding option of default value to functor for relation tables, !3302 (@tnanut)
- ~"MC checking" | Easier to read logging for some tools in Phys/MCAssociation, !3323 (@erodrigu)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding | Add DetDesc specific ref for PlumeReco.plume_decoding test, !3308 (@jonrob)
- ~RICH | Fix RICH refs following making dd4hep default build, !3298 (@jonrob)
- ~Build | Remove ignore subobject linkage warnings which have been fixed by gaudi/Gaudi!1425, !3318 (@graven)
- Prefer LinkedTo when iterating over LinksByKey, !3319 (@graven)
- Fix: Remove WARNING Message from WeightedRelTableAlg, !3306 (@tfulghes)
- Remove LokiProtoParticles, !3304 (@graven)


### Documentation ~Documentation

- ~Functors | Add docstrings for functors, !3241 (@tfulghes)
- Follows lhcb/Moore!1996, changes in Functor's reference page, !3307 (@tfulghes)

### Other

- ~Decoding ~RICH | Fix Rich/RichFutureRecSys/examples/RichDecode.py with dd4hep, !3328 (@jonrob)
- ~Tracking | Use FastMomentaEstimation tool for VeloMuon tracks, !3234 (@peilian)
- ~Calo ~"Event model" | Adds functor for brem-corrected momenta / covariance, !3233 (@mveghel)
- ~Functors ~Tuples | Add more functors to access neutral CALO information, !3292 (@jzhuo)
- ~Functors ~Tuples | New thor functors for differents types of covariance matrix, !3281 (@jzhuo)
- Remove duplicated pytest (as qmt test), !3334 (@rmatev)
- Fix stand-alone compilation of public headers, !3331 (@clemenci)
- Add missing const, !3317 (@graven)
- Fix my last-minute change to !3302, !3315 (@rmatev)
- Follow !3307, !3313 (@rmatev)
- Follow up !3307, !3312 (@rmatev)
- Follow LHCb!3954, !3311 (@rmatev)
- Revert !3308, !3309 (@rmatev)
- Update References for: LHCb!3953, Moore!2053, DaVinci!809 based on lhcb-master-mr/6955, !3305 (@lhcbsoft)
- Cleanup DaVinci Filters, !3303 (@graven)
- Correct representation for TRACKISSELECTED, !3300 (@mwilkins)
- Use ConditionAccessor in DVCommonBase rather than calling retrieveObject from detSvc, !3287 (@sponce)
- MicroDST: remove unused code, !3297 (@graven)
- Add the possibility to plot the residual distribution per module in FTTrackMonitor, !3293 (@gtuci)
- Declare PrLHCbID2MCParticleVPFTMU, !3291 (@cburr)
- Fix tests with ROOT 6.26, !3278 (@clemenci)
- Support VeloMuon, MuonUT and SeedMuon tracks in TrackMasterFitter, !3248 (@ausachov)
