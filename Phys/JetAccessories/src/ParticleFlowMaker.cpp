/*****************************************************************************\
 * * (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "Event/Particle.h"
#include "JetUtils.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>

/*

Receives a vector of LHCb::Particle containers, basic  or composite, outputs a list of particles
removing duplicities (no protoparticle is used more than once)

For each composite particle, if the protoparticles associated to their
daughters are not in the list of used protoparticles yet, they are added and the composite particle is saved in the
output list.

For each basic particle, if the protoparticle associated to it is not in the list of used protoparticles,
it is added there and the particle saved in the list of output particles.

The used protparticles are separated in neutral, positive ans negative charges containers, to speed up processing.

TODO: The test performed to avoid double counting is done by using the particle's address.
We plan to include LoKi::CheckOverlap for next version.

*/

namespace LHCb {
  namespace {
    class ProtoSet {
      std::array<std::set<const ProtoParticle*>, 3> protos;
      static constexpr int                          idx( int charge ) { return charge == 0 ? 0 : charge < 0 ? 1 : 2; }

    public:
      auto insert( const Particle& pp ) {
        if ( !pp.proto() )
          throw GaudiException( "Particle has no ProtoParticle", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        return protos[idx( pp.charge() )].insert( pp.proto() );
      }
      bool contains( const Particle& p ) const {
        auto const& s = protos[idx( p.charge() )];
        return s.find( p.proto() ) != s.end();
      }
    };
  } // namespace

  using inputs = const Gaudi::Functional::vector_of_const_<Particles>;

  class ParticleFlowMaker : public Algorithm::MergingTransformer<Particle::Selection( inputs const& )> {
    mutable Gaudi::Accumulators::Counter<>     m_count{this, "00: # Number of Events"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputChargedsCounter{this, "01: # Basic Charged Input Particles"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputNeutralsCounter{this, "02: # Basic Neutral Input Particles"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputCompositesCounter{this, "03: # Composite Input Particles"};

    mutable Gaudi::Accumulators::StatCounter<> m_nbChargedsCounter{this, "04: # Basic Charged Output Particles"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbNeutralsCounter{this, "05: # Basic Neutral Output Particles"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbCompositesCounter{this, "06: # Composite Output Particles"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbParticleCounter{this, "07: # Merged Output Particles"};

    mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedBasicsCounter{
        this, "08: # ProtoParticles from Basic Particles rejected"};
    mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedCompositesCounter{
        this, "09: # ProtoParticles from Composite Particles rejected"};

  public:
    /// Constructor.
    ParticleFlowMaker( const std::string& name, ISvcLocator* svc )
        : MergingTransformer( name, svc, {"Inputs", {}}, {"Output", "Phys/ParticleFlow/Particles"} ) {}

    // Main method
    Particle::Selection operator()( inputs const& Inputs ) const override {

      ++m_count;

      // Input can be in any order. Separate into categories (composites, basic_positives, basic_negatives and
      // neutrals). Already process particles whose daughter shoud be rejected

      Particle::ConstVector composites;
      Particle::ConstVector basics;

      for ( auto& Input : Inputs ) {
        if ( Input.empty() ) { continue; }
        // Enough to interrogate the first particle in each Input list. Assumes same type
        // of Particles in each Input
        if ( ( *Input.begin() )->isBasicParticle() )
          for ( auto prt : Input ) basics.emplace_back( prt );
        else
          for ( auto prt : Input ) composites.emplace_back( prt );
      }
      m_nbInputCompositesCounter += composites.size();

      Particle::Selection prts;

      // Now process the composite particles
      ProtoSet protos{};
      for ( auto prt : composites ) {
        auto dtrs = JetAccessories::getBasics( *prt );
        // A composite Particle is not added to the output list if at least one of their daughther protoparticles is
        // already used
        if ( JetAccessories::any_of( dtrs, [&]( const auto& dtr ) { return protos.contains( *dtr ); } ) ) {
          // Some protoparticle from this composite particle was already used
        } else {
          for ( auto dtr : dtrs ) protos.insert( *dtr );
          prts.insert( prt ); // Insert composite particles to the output list
        }
      }
      int nbCompositesOut      = prts.size();
      int nbRejectedComposites = composites.size() - prts.size();
      m_nbRejectedCompositesCounter += nbRejectedComposites;
      m_nbCompositesCounter += nbCompositesOut;

      // Now look at the basic particles ...
      int nbChargedsIn     = 0;
      int nbNeutralsIn     = 0;
      int nbNeutralsOut    = 0;
      int nbChargedsOut    = 0;
      int nbRejectedBasics = 0;

      for ( auto prt : basics ) {
        ++( prt->charge() == 0 ? nbNeutralsIn : nbChargedsIn );
        if ( protos.insert( *prt ).second ) {
          prts.insert( prt );
          ++( prt->charge() == 0 ? nbNeutralsOut : nbChargedsOut );
        } else {
          ++nbRejectedBasics;
        }
      }

      m_nbInputChargedsCounter += nbChargedsIn;
      m_nbInputNeutralsCounter += nbNeutralsIn;
      m_nbNeutralsCounter += nbNeutralsOut;
      m_nbChargedsCounter += nbChargedsOut;
      m_nbRejectedBasicsCounter += nbRejectedBasics;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Output: #composites: " << nbCompositesOut << " #chargeds: " << nbChargedsOut
                << " #neutrals: " << nbNeutralsOut << " Rejected composites: " << nbRejectedComposites
                << " Rejected basics: " << nbRejectedBasics << endmsg;
        debug() << "Number of output particles: " << prts.size() << endmsg;
      }

      m_nbParticleCounter += prts.size();

      return prts;
    }
  };

  DECLARE_COMPONENT_WITH_ID( ParticleFlowMaker, "ParticleFlowMaker" )

} // namespace LHCb
