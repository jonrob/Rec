###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest

import GaudiPython
from GaudiPython.Bindings import AppMgr
import PartProp.Service
from Configurables import LHCbApp
from Configurables import MessageSvc

MessageSvc(OutputLevel=1)

app = LHCbApp(OutputLevel=1)
app.DataType = "Upgrade"

app = AppMgr()
pps = app.ppSvc()
toolsvc = app.toolSvc()
app.initialize()

Decay = GaudiPython.gbl.Decays.Decay


@pytest.fixture()
def preambulo():
    decoder = toolsvc.create(
        "DecodeSimpleDecayString",
        interface=GaudiPython.gbl.IDecodeSimpleDecayString)

    return decoder


def test_descriptor_intact(preambulo):
    decoder = preambulo
    descriptor = "[B+ -> K+ pi- mu+ e- p+ gamma pi0]cc"

    decoder.setDescriptor(descriptor)
    dec = Decay()

    sc = decoder.getDecay(dec)
    print(dec, ":", sc.message())
    assert sc
    sc = decoder.getDecay_cc(dec)
    print(dec, ":", sc.message())
    assert sc


def test_descriptor_bad_daughter(preambulo):
    decoder = preambulo
    descriptor = "[B+ -> K+ pi- mu+ e- unicorn gamma pi0]cc"

    with pytest.raises(GaudiPython.gbl.GaudiException):
        decoder.setDescriptor(descriptor)


def test_descriptor_bad_mother(preambulo):
    decoder = preambulo
    descriptor = "[squirrel -> K+ pi- mu+ e- p+ gamma pi0]cc"

    with pytest.raises(GaudiPython.gbl.GaudiException):
        decoder.setDescriptor(descriptor)
