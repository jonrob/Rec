/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/ILifetimeFitter.h"

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/Interface.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/VertexHolder.h"

/**
 *  Collection of LoKi-functors, dealing with "LifeTime"-fitter
 *  @see ILifetimeFitter
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2008-01-17
 */
namespace LoKi {

  namespace Particles {

    /** @class LifeTime
     *  The simple function which evaliuates the lifetime of the particle
     *  using ILifetimeFitter tool
     *
     *  @see LoKi::Cuts::LTIME
     *  @see LoKi::Cuts::LIFETIME
     *  @see ILifetimeFitter
     *
     *  @attention Please mind the units! Unlike all other "time" quantities in LoKi,
     *             this functor returns "time"-units instead of "length"-units!
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    class GAUDI_API LifeTime : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                               public virtual LoKi::AuxDesktopBase,
                               public LoKi::Vertices::VertexHolder {
    public:
      /// constructor
      LifeTime( const IDVAlgorithm* algorithm, const ILifetimeFitter* tool, const LHCb::VertexBase* vertex,
                const double chi2cut = -1 );
      /// MANDATORY: the destructor
      virtual ~LifeTime();
      /// MANDATORY: clone method ("virtual constructor")
      LifeTime* clone() const override { return new LifeTime( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return lifeTime( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

      // set the fitter
      void setTool( const ILifetimeFitter* tool ) const { m_fitter = tool; }
      // set the fitter
      void setTool( const LoKi::Interface<ILifetimeFitter>& tool ) const { m_fitter = tool; }
      // get the fitter
      const ILifetimeFitter* tool() const { return m_fitter; }
      // get the fitter
      const ILifetimeFitter* fitter() const { return m_fitter; }
      // cast to the fitter
      operator const LoKi::Interface<ILifetimeFitter>&() const { return m_fitter; }
      /// the embedded chi20cut
      double chi2cut() const { return m_chi2cut; }

      /** evaluate the lifetime
       *  @attention apply embedded chi2-cut
       */
      result_type lifeTime( argument p, IGeometryInfo const& geometry ) const;
      /** evaluate the lifetime chi2
       *  @attention apply embedded chi2-cut
       */
      result_type lifeTimeChi2( argument p, IGeometryInfo const& geometry ) const;
      /** evaluate the lifetime signed chi2
       *  @attention apply embedded chi2-cut
       */
      result_type lifeTimeSignedChi2( argument p, IGeometryInfo const& geometry ) const;
      /// evaluate the lifetime fit chi2
      result_type lifeTimeFitChi2( argument p, IGeometryInfo const& geometry ) const;
      /// evaluate the lifetime error
      result_type lifeTimeError( argument p, IGeometryInfo const& geometry ) const;

    private:
      /// the lifetime fitter itself
      mutable LoKi::Interface<ILifetimeFitter> m_fitter; // the lifetime fitter
      /// the embedded chi2 cut
      double m_chi2cut; // chi2 cut
    };

    /** @class LifeTimeChi2
     *  The simple function which evaliuates the chi2 for lifetime of the particle
     *  using ILifetimeFitter tool
     *
     *  @see LoKi::Cuts::LTCHI2
     *  @see LoKi::Cuts::LTIMECHI2
     *  @see LoKi::Cuts::LIFETIMECHI2
     *  @see ILifetimeFitter
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    class GAUDI_API LifeTimeChi2 : public LifeTime {
    public:
      /// constructor
      LifeTimeChi2( IDVAlgorithm const* algorithm, const ILifetimeFitter* tool, const LHCb::VertexBase* vertex,
                    const double chi2cut = -1 )
          : LoKi::AuxDesktopBase( algorithm ), LifeTime( algorithm, tool, vertex, chi2cut ) {}
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeChi2* clone() const override { return new LifeTimeChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return lifeTimeChi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeSignedChi2
     *  The simple function which evaliuates the chi2 for lifetime of the particle
     *  using ILifetimeFitter tool
     *
     *  @see LoKi::Cuts::LTSIGNCHI2
     *  @see LoKi::Cuts::LTIMESIGNCHI2
     *  @see LoKi::Cuts::LIFETIMESIGNCHI2
     *  @see ILifetimeFitter
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeSignedChi2 : LoKi::Particles::LifeTimeChi2 {

      /// constructor
      LifeTimeSignedChi2( IDVAlgorithm const* algorithm, const ILifetimeFitter* tool, const LHCb::VertexBase* vertex,
                          const double chi2cut = -1 )
          : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::LifeTimeChi2( algorithm, tool, vertex, chi2cut ) {}
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeSignedChi2* clone() const override { return new LifeTimeSignedChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return lifeTimeSignedChi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeFitChi2
     *  The simple function which evaliuates the chi2 for lifetime
     *  fit of the particle
     *  using ILifetimeFitter tool
     *
     *  @see LoKi::Cuts::LTFITCHI2
     *  @see LoKi::Cuts::LTIMEFITCHI2
     *  @see LoKi::Cuts::LIFETIMEFITCHI2
     *  @see ILifetimeFitter
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeFitChi2 : LoKi::Particles::LifeTime {

      /// constructor
      LifeTimeFitChi2( IDVAlgorithm const* algorithm, const ILifetimeFitter* tool, const LHCb::VertexBase* vertex )
          : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::LifeTime( algorithm, tool, vertex ) {}
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeFitChi2* clone() const override { return new LoKi::Particles::LifeTimeFitChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return lifeTimeFitChi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeError
     *  The simple function which evaluates the lifetime uncertanty
     *  using ILifetimeFitter tool
     *
     *  @see LoKi::Cuts::LTIMEERR
     *  @see ILifetimeFitter
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeError : LoKi::Particles::LifeTime {

      /// constructor
      LifeTimeError( IDVAlgorithm const* algorithm, const ILifetimeFitter* tool, const LHCb::VertexBase* vertex )
          : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::LifeTime( algorithm, tool, vertex ) {}
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeError* clone() const override { return new LoKi::Particles::LifeTimeError( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return lifeTimeError( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "LTIMEERR"; }
    };

  } // namespace Particles

} // namespace LoKi
