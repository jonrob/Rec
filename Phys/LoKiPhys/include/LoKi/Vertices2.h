/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/Particles4.h"

/**
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-11-25
 */
namespace LoKi {

  namespace Vertices {

    /** @class  ImpPar
     *
     *  evaluator of the impact parameter of vertex
     *  with respect to a particle
     *
     *  @see LHCb::VertexBase
     *  @see LHCb::Particle
     *  @see LoKi::Particles::ImpPar
     *  @see LoKi::Cuts::VIP
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2003-03-17
     */
    class GAUDI_API ImpPar : public LoKi::BasicFunctors<const LHCb::VertexBase*>::Function,
                             virtual LoKi::AuxDesktopBase {
    public:
      /// constructor from the particle and tool
      ImpPar( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
              const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle and tool
      ImpPar( IDVAlgorithm const* algorithm, const LHCb::Particle* particle, const LoKi::Vertices::ImpParBase& base );
      /// constructor from the particle and tool
      ImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
              const LHCb::Particle* particle );
      /// constructor from the particle and tool
      ImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpParBase& base, const LHCb::Particle* particle );
      /// copy constructor
      ImpPar( IDVAlgorithm const* algorithm, const ImpPar& right );
      /// MANDATORY: clone method ("virtual constructor")
      ImpPar* clone() const override;
      /// MANDATORY: virtual destructor
      ~ImpPar();
      /// MANDATORY: the only one essential method
      result_type operator()( argument ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

      /// set the particle
      void setParticle( const LHCb::Particle* p ) { m_particle = p; }

    protected:
      /// accessor to the particle
      const LHCb::Particle* particle() const { return m_particle; }

    private:
      // default constructor is private
      ImpPar();

      LoKi::Particles::ImpPar m_evaluator;
      const LHCb::Particle*   m_particle;
    };

    /** @class  ImpParChi2
     *
     *  evaluator of the impact parameter of vertex
     *  with respect to a particle
     *
     *  @see LHCb::VertexBase
     *  @see LHCb::Particle
     *  @see LoKi::Particles::ImpParChi2
     *  @see LoKi::Cuts::VIPCHI2
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2003-03-17
     */
    class GAUDI_API ImpParChi2 : public LoKi::BasicFunctors<const LHCb::VertexBase*>::Function,
                                 virtual LoKi::AuxDesktopBase {
    public:
      /// constructor from the particle and tool
      ImpParChi2( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                  const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle and tool
      ImpParChi2( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                  const LoKi::Vertices::ImpParBase& base );
      /// constructor from the particle and tool
      ImpParChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                  const LHCb::Particle* particle );
      /// constructor from the particle and tool
      ImpParChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpParBase& base,
                  const LHCb::Particle* particle );
      /// copy constructor
      ImpParChi2( IDVAlgorithm const* algorithm, const ImpParChi2& right );
      /// MANDATORY: clone method ("virtual constructor")
      ImpParChi2* clone() const override;
      /// MANDATORY: virtual destructor
      ~ImpParChi2();
      /// MANDATORY: the only one essential method
      result_type operator()( argument ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      // set the particle
      void setParticle( const LHCb::Particle* p ) { m_particle = p; }

    protected:
      /// accessor to the particle
      const LHCb::Particle* particle() const { return m_particle; }

    private:
      // default constructor is private
      ImpParChi2();

      LoKi::Particles::ImpParChi2 m_evaluator;
      const LHCb::Particle*       m_particle;
    };

  } // namespace Vertices

} //                                                      end of namespace LoKi
