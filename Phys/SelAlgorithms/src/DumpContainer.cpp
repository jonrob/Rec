/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SelAlgorithms/DumpContainer.h"
#include "Event/Particle_v2.h"
#include "Event/PrLongTracks.h"
#include "Event/Track_v3.h"

DECLARE_COMPONENT_WITH_ID( Dumping::DumpContainer<LHCb::Event::v3::Tracks>, "DumpContainer__SOATracks" )
DECLARE_COMPONENT_WITH_ID( Dumping::DumpContainer<LHCb::Pr::Long::Tracks>, "DumpContainer__PrLongTracks" )
