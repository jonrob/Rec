/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/MCTaggingHelper.h"

namespace LHCb::FlavourTagging {

  std::ostream& operator<<( std::ostream& os, originTagCodes code ) {
    switch ( code ) {
    case originTagCodes::SignalB_Decay:
      return os << "SignalB_Decay";
    case originTagCodes::SS_Fragmentation:
      return os << "SS_Fragmentation";
    case originTagCodes::OS_B_Decay:
      return os << "OS_B_Decay";
    case originTagCodes::OS_ExcitedB_Decay:
      return os << "OS_ExcitedB_Decay";
    case originTagCodes::OS_b_Fragmentation:
      return os << "OS_b_Fragmentation";
    case originTagCodes::Prompt_From_SignalB_PV:
      return os << "Prompt_From_SignalB_PV";
    case originTagCodes::Other_From_SignalB_PV:
      return os << "Other_From_SignalB_PV";
    case originTagCodes::Not_From_SignalB_PV:
      return os << "Not_From_SignalB_PV";
    }
    return os << "LHCb::FlavourTagging::originTagCode " << static_cast<int>( code ) << " unknown";
  };

  originTagCodes originType( const LHCb::MCParticle& bMC, const LHCb::MCParticle& bAnMC, const LHCb::MCParticle& tMC ) {

    const LHCb::MCVertex* bPV = bMC.primaryVertex();
    const LHCb::MCVertex* tPV = tMC.primaryVertex();
    if ( bPV != tPV ) return originTagCodes::Not_From_SignalB_PV; // not from same PV

    // go through tagging particle ancestors
    const LHCb::MCParticle*              tAnMC( &tMC );
    std::vector<const LHCb::MCParticle*> decayChain;
    while ( tAnMC->mother() ) {
      tAnMC = tAnMC->mother();
      decayChain.push_back( tAnMC );

      if ( tAnMC == &bMC ) return originTagCodes::SignalB_Decay; // from signal B decay
    }

    bool hasUnexcitedBMother = false;
    bool hasExcitedBMother   = false;
    for ( long unsigned int i = 0; i + 1 < decayChain.size(); i++ ) {
      int pid = std::abs( decayChain[i]->particleID().pid() );
      if ( pid == 511 || pid == 521 || pid == 531 || pid == 541 ) {
        hasUnexcitedBMother = true;
      } else if ( decayChain[i]->particleID().hasBottom() ) {
        hasExcitedBMother = true;
      }
    }
    if ( tAnMC->particleID().hasBottom() ) { // some kind of OS/SS business

      if ( tAnMC == &bAnMC ) return originTagCodes::SS_Fragmentation; // SS fragmentation

      if ( hasUnexcitedBMother ) return originTagCodes::OS_B_Decay; // OS decay

      if ( hasExcitedBMother ) return originTagCodes::OS_ExcitedB_Decay; // OS decay from excited B

      return originTagCodes::OS_b_Fragmentation; // OS fragmentation from b quark
    }
    if ( hasUnexcitedBMother || hasExcitedBMother ) return originTagCodes::Other_From_SignalB_PV;

    return originTagCodes::Prompt_From_SignalB_PV; // prompt tracks
  }

  originTagCodes originType( const LHCb::MCParticle& bMC, const LHCb::MCParticle& tMC ) {
    const LHCb::MCParticle* bAnMC( &bMC );
    while ( bAnMC->mother() ) { bAnMC = bAnMC->mother(); }
    return originType( bMC, *bAnMC, tMC );
  }
}; // namespace LHCb::FlavourTagging
