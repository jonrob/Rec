/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MuonPIDs_v2.h"
#include "Event/Track_v3.h"
#include "SelKernel/VertexRelation.h"

namespace LHCb::Event::v3 {
  using TracksWithPVs    = LHCb::Event::zip_t<LHCb::Event::v3::Tracks, BestVertexRelations>;
  using TracksWithMuonID = LHCb::Event::zip_t<LHCb::Event::v3::Tracks, LHCb::Event::v2::Muon::PIDs>;
} // namespace LHCb::Event::v3
