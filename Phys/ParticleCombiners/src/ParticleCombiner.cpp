/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombKernel/ParticleCombiner.h"

using TwoBodyCombiner   = NBodyParticleCombiner<LHCb::Particle, LHCb::Particle>;
using ThreeBodyCombiner = NBodyParticleCombiner<LHCb::Particle, LHCb::Particle, LHCb::Particle>;
using FourBodyCombiner  = NBodyParticleCombiner<LHCb::Particle, LHCb::Particle, LHCb::Particle, LHCb::Particle>;
using FiveBodyCombiner =
    NBodyParticleCombiner<LHCb::Particle, LHCb::Particle, LHCb::Particle, LHCb::Particle, LHCb::Particle>;
using SixBodyCombiner   = NBodyParticleCombiner<LHCb::Particle, LHCb::Particle, LHCb::Particle, LHCb::Particle,
                                              LHCb::Particle, LHCb::Particle>;
using SevenBodyCombiner = NBodyParticleCombiner<LHCb::Particle, LHCb::Particle, LHCb::Particle, LHCb::Particle,
                                                LHCb::Particle, LHCb::Particle, LHCb::Particle>;

DECLARE_COMPONENT_WITH_ID( TwoBodyCombiner, "TwoBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( ThreeBodyCombiner, "ThreeBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( FourBodyCombiner, "FourBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( FiveBodyCombiner, "FiveBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( SixBodyCombiner, "SixBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( SevenBodyCombiner, "SevenBodyCombiner" )
