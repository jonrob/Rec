/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H
#define PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H 1

// from STL
#include <vector>

class ITaggingClassifier {
public:
  virtual ~ITaggingClassifier() = default;
  /**
   * @brief      Main classification method
   *
   * Takes a vector of values of features and returns the corresponding MVA
   * output.
   *
   * @param[in]  featureValues  A vector of feature values
   *
   * @return     MVA classifier value
   */
  virtual double getClassifierValue( const std::vector<double>& featureValues ) = 0;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H
