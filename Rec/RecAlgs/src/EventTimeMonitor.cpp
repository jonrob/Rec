/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "Event/ODIN.h" // event & run number
#include "GaudiAlg/GaudiHistoAlg.h"
#include "LHCbAlgs/Consumer.h"

namespace {
  //@FIXME: what about leapyears?
  static const std::array<unsigned int, 12> month_offsets = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};
} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : EventTimeMonitor
//
// 2012-04-19 : Patrick Koppenburg
//-----------------------------------------------------------------------------

/** @class EventTimeMonitor EventTimeMonitor.h
 *
 *  Creates histograms of event time
 *
 *  @author Patrick Koppenburg
 *  @date   2012-04-19
 */

class EventTimeMonitor final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN& ),
                                                                Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /// Standard constructor
  EventTimeMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                          ///< Algorithm initialization
  void       operator()( const LHCb::ODIN& ) const override; ///< Algorithm execution

private:
  /// the histogram definition (as property)
  Gaudi::Property<Gaudi::Histo1DDef> m_histoS{
      this, "SecondsPlot", {"GPS Seconds", 0, 3600., 3600}, "The parameters of 'delta memory' histogram"};
  Gaudi::Histo1DDef           m_histoH{"GPS Hours", -0.5, 23.5, 24};    // the histogram definition (as property)
  Gaudi::Histo1DDef           m_histoD{"GPS Days", 0.5, 365.5, 366};    // the histogram definition (as property)
  Gaudi::Histo1DDef           m_histoY{"GPS Year", 2008.5, 2028.5, 20}; // the histogram definition (as property)
  mutable AIDA::IHistogram1D* m_plotS = nullptr;                        // the histogram of seconds
  mutable AIDA::IHistogram1D* m_plotH = nullptr;                        // the histogram of hours
  mutable AIDA::IHistogram1D* m_plotD = nullptr;                        // the histogram of day of year
  mutable AIDA::IHistogram1D* m_plotY = nullptr;                        // the histogram of year
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EventTimeMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
EventTimeMonitor::EventTimeMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"Input", LHCb::ODINLocation::Default} ) {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode EventTimeMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already by GaudiHistoAlg

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  if ( produceHistos() ) {
    m_plotS = book( m_histoS );
    m_plotH = book( m_histoH );
    m_plotD = book( m_histoD );
    m_plotY = book( m_histoY );
  }
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
void EventTimeMonitor::operator()( const LHCb::ODIN& odin ) const {

  if ( produceHistos() ) {
    const Gaudi::Time gtime = odin.eventTime();
    m_plotY->fill( gtime.year( false ) );
    m_plotD->fill( month_offsets[gtime.month( false )] + gtime.day( false ) );
    m_plotH->fill( gtime.hour( false ) );
    m_plotS->fill( 60 * gtime.minute( false ) + gtime.second( false ) + gtime.nsecond() / 1000000000. );
  }
}

//=============================================================================
