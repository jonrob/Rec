/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Features.h"
#include "LHCbMath/VectorizedML/Evaluators.h"
#include "LHCbMath/VectorizedML/Sequence.h"

namespace ProbNN {

  using namespace ProbNN::Features;

  namespace Long {

    namespace Electron {

      namespace details {
        struct Features final
            : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, InEcal, ElectronShowerEoP,
                                           ElectronShowerDLL, ElectronHypoEoP, HcalPIDe, BremPIDe, RichDLLe, RichDLLmu,
                                           RichDLLk, RichDLLp, usedRich1Gas, usedRich2Gas, InMuon, LogMuonChi2> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = nInput * 2;
        constexpr int hidden_layer2 = int( hidden_layer * 0.6 );
        constexpr int hidden_layer3 = int( hidden_layer2 * 0.33 );
        constexpr int nOutput       = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, hidden_layer3> {};
        struct A3 : ReLU<hidden_layer3> {};
        struct L4 : Linear<hidden_layer3, nOutput> {};
        struct A4 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3, L4, A4> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Electron

    namespace Muon {

      namespace details {
        struct Features final
            : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, InMuon, IsMuon, LogMuonChi2, MuonCatBoost,
                                           RichDLLe, RichDLLmu, RichDLLk, RichDLLp, usedRich1Gas, usedRich2Gas,
                                           ElectronShowerDLL, EcalPIDmu, HcalPIDmu> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = int( nInput * 2.25 );
        constexpr int hidden_layer2 = int( hidden_layer * 0.6 );
        constexpr int hidden_layer3 = int( hidden_layer2 * 0.3 );
        constexpr int nOutput       = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, hidden_layer3> {};
        struct A3 : ReLU<hidden_layer3> {};
        struct L4 : Linear<hidden_layer3, nOutput> {};
        struct A4 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3, L4, A4> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Muon

    namespace Pion {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, RichDLLe, RichDLLmu,
                                                             RichDLLk, RichDLLp, usedRich1Gas, usedRich2Gas, EcalPIDe,
                                                             InHcal, HcalEoP, InMuon, IsMuon, LogMuonChi2> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = int( nInput * 2.25 );
        constexpr int hidden_layer2 = int( hidden_layer * 0.4 );
        constexpr int hidden_layer3 = int( hidden_layer2 * 0.2 );

        constexpr int nOutput = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, hidden_layer3> {};
        struct A3 : ReLU<hidden_layer3> {};
        struct L4 : Linear<hidden_layer3, nOutput> {};
        struct A4 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3, L4, A4> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Pion

    namespace Kaon {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, RichDLLe, RichDLLmu,
                                                             RichDLLk, RichDLLp, usedRich1Gas, usedRich2Gas, EcalPIDe,
                                                             InHcal, HcalEoP, InMuon, IsMuon, LogMuonChi2> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = int( nInput * 1.5 );
        constexpr int hidden_layer2 = int( hidden_layer * 0.8 );
        constexpr int hidden_layer3 = int( hidden_layer2 * 0.33 );
        constexpr int nOutput       = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, hidden_layer3> {};
        struct A3 : ReLU<hidden_layer3> {};
        struct L4 : Linear<hidden_layer3, nOutput> {};
        struct A4 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3, L4, A4> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Kaon

    namespace Proton {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, RichDLLe, RichDLLmu,
                                                             RichDLLk, RichDLLp, usedRich1Gas, usedRich2Gas, EcalPIDe,
                                                             InHcal, HcalEoP, InMuon, IsMuon, LogMuonChi2> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = int( nInput * 1.75 );
        constexpr int hidden_layer2 = int( hidden_layer * 0.4 );
        constexpr int nOutput       = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, nOutput> {};
        struct A3 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Proton

    namespace Ghost {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackChi2PerDoF, TrackGhostProb, RichDLLe, RichDLLmu,
                                                             RichDLLk, RichDLLp, RichDLLbt, usedRich1Gas, usedRich2Gas,
                                                             EcalPIDe, InHcal, HcalEoP, InMuon, IsMuon, LogMuonChi2> {};

        constexpr int nInput        = Features::Size;
        constexpr int hidden_layer  = int( nInput * 1.75 );
        constexpr int hidden_layer2 = int( hidden_layer * 0.4 );
        constexpr int nOutput       = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, hidden_layer2> {};
        struct A2 : ReLU<hidden_layer2> {};
        struct L3 : Linear<hidden_layer2, nOutput> {};
        struct A3 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2, L3, A3> {};

      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Ghost

  } // namespace Long

  namespace Upstream {

    namespace Electron {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu, BremPIDe> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 21;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Electron

    namespace Muon {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 17;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Muon

    namespace Pion {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu, BremPIDe> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 19;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Pion

    namespace Kaon {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu, BremPIDe> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 24;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Kaon

    namespace Proton {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu, BremPIDe> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 23;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};
      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Proton

    namespace Ghost {

      namespace details {
        struct Features final : LHCb::VectorizedML::Features<TrackGhostProb, TrackChi2PerDoF, RichDLLbt, RichDLLp,
                                                             RichDLLk, RichDLLe, RichDLLmu, BremPIDe> {};

        constexpr int nInput       = Features::Size;
        constexpr int hidden_layer = 19;
        constexpr int nOutput      = 1;

        using namespace LHCb::VectorizedML::Layers;
        struct L1 : Linear<nInput, hidden_layer> {};
        struct A1 : ReLU<hidden_layer> {};
        struct L2 : Linear<hidden_layer, nOutput> {};
        struct A2 : Sigmoid<nOutput> {};

        struct MLP : LHCb::VectorizedML::Sequence<nInput, nOutput, L1, A1, L2, A2> {};

      } // namespace details

      struct Model final : LHCb::VectorizedML::EvaluatorOnRange<details::MLP, details::Features> {};

    } // namespace Ghost

  } // namespace Upstream

} // namespace ProbNN
